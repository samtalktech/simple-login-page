import React, { Component } from 'react';
import './assets/css/mobilenav.css';
import logos from './assets/img/logo.jpg';


class MobileNav extends Component {
    render(){
    return(
          

        <div className="navbar" >

            <ul className="mobile-navigation">
                <li><a href='#'>Home</a></li>
                <li><a href='#'>Products</a></li>
                <li><a href='#'>Services</a></li>
                <li><a href='#'>About Us</a></li>
                <li><a href='#'>Locate us</a></li>
            </ul>



        </div>
    )
    }
}

export  default MobileNav;
