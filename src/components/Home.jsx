import React, { Component } from 'react';
import Navbar from './Navbar';
import MobileNav from './MobileNav'
import ContactIcon from './assets/img/contacticon.png'
import Counter from './Counter';
import './assets/css/home.css';
import Dashboard from './Dashboard';




class LoginPage extends Component {
state ={

    names : 'Login',
    navToggle : true,
    onBoardToggle : true,
    user : '',
    email : '',
    password : '',
    regPageName : 'REGISTERATION PAGE',
    logPageName : 'LOGIN PAGE'
}

// handleLogin = () => {
//   fire
//   .auth()
//   .signInWithEmailAndPassword(email,password)
//   .catch(err=>{
//     switch(err.code){
//       case "auth/invalid-email":
//       case "auth/user-disabled":
//       case "auth/user-not-found":
//         setEmailError(err.msg);
//         break;
//         case "auth/wrong-password":
//           setPasswordError(err.message);
//           break;
//     }
//   })

// }

// handSignup = () => {
//   fire
//   .auth()
//   .signInWithEmailAndPassword(email,password)
//   .catch(err=>{
//     switch(err.code){
//       case "auth/invalid-email":
//       case "auth/user-disabled":
//       case "auth/user-not-found":
//         setEmailError(err.msg);
//         break;
//         case "auth/wrong-password":
//           setPasswordError(err.message);
//           break;
//     }
//   })

// }

onMobileNav = () => {
    this.setState({navToggle : !this.state.navToggle})
    console.log("pressed")
  }


toggleRegandLog =(e)=>{
  e.preventDefault()
  this.setState({onBoardToggle : !this.state.onBoardToggle})
}


    render() { 

       
        return (
          
                 
        <div className="LoginPage">
            <Navbar checkHamButton={this.onMobileNav}/>
           
           <ul className={this.state.navToggle? 'mobile-navigation-off' : 'mobile-navigation'}>
                <li><a href='#'>Home</a></li>
                <li><a href='#'>Product</a></li>
                <li><a href='#'>Services</a></li>
                <li><a href='#'>About Us</a></li>
                <li><a href='#'>Locate us</a></li>
            </ul>
            
            <p className="login-page-title">{this.state.logPageName}</p>
           
            
            <div className='box'>

                <img className="image-place-holder" src={ContactIcon}/>
                
                <form className= {this.state.onBoardToggle ?'login-form' :'login-form-off'} id="login" method="post">
                    <input className="inputFields" placeholder="Email" type="email"/>
                    <input className="inputFields" placeholder="Password" type="password"/>
                    <span className="forgot-password"><a href="#">Forgot Password?</a></span>
                    <button className="success" type="submit">Login</button>
                 </form>

                 <form className= {this.state.onBoardToggle ?'register-form-off' : 'register-form'} id="register" method="post">
                    <input className="inputFields" placeholder="Email" type="email"/>
                    <input className="inputFields" placeholder="Username" type="text"/>
                    <input className="inputFields" placeholder="Password" type="password"/>
                    <input className="inputFields" placeholder="Confirm Password" type="password"/>
                    <button className="success" type="submit">Sign up</button>
                 </form>

              

                 <span className="registeration-link"><label>{this.state.onBoardToggle?'New User?' : 'Existing User?'}</label><a href="" onClick={(event)=>this.toggleRegandLog(event)}>{this.state.onBoardToggle? 'Register' : 'Login'}</a></span>

                
            
            
            </div>
        </div>
        )

    
}
}
 
export default LoginPage;