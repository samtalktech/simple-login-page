import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAWqS7ia2dJyOIgBn31b8TeDfXoG8PD3u8",
    authDomain: "login-7ad43.firebaseapp.com",
    projectId: "login-7ad43",
    storageBucket: "login-7ad43.appspot.com",
    messagingSenderId: "820752307971",
    appId: "1:820752307971:web:bc2f26cb73054e37857b36"
  };

  // Initialize Firebase
  const fire = initializeApp(firebaseConfig);

  export default fire;