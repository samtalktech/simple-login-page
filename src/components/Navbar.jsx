import React, { Component } from 'react';
import './assets/css/navbar.css';
import logos from './assets/img/logo.jpg';


class Navbar extends Component {
    render(){
    return(
          

        <div className="navbar" >

            <img src={logos} alt ="logo" className="logo" />

            <ul className="desktop-navigation">
                <li><a href='#'>Home</a></li>
                <li><a href='#'>Products</a></li>
                <li><a href='#'>Services</a></li>
                <li><a href='#'>About Us</a></li>
                <li><a href='#'>Locate us</a></li>
            </ul>

           

        <div className="hamburger" onClick={this.props.checkHamButton}>
                <div></div>
                <div></div>
                <div></div>
            </div>



        </div>
    )
    }
}

export  default Navbar;
