import React, { Component } from 'react';
import './assets/css/counter.css';

class Counters extends React.Component {

    state ={
        count : 0

    };

    increament =()=>{
        this.setState({count : this.state.count + 1})
    }

      reducer =()=>{
        this.setState({count : this.state.count - 1})
    }
    
    render() { 
        
        return (
        <div className="Counters">

            <span className="display-box">
                {this.state.count === 0? 'Zero' : this.state.count}
            </span>

            <button id="success" onClick={this.increament}>Increment</button>
            <button id="reduce" onClick={this.reducer}>Reduce</button>
            

        </div>);
    }
}
 
export default Counters;